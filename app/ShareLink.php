<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareLink extends Model
{
    protected $fillable = [
        'url'
    ];

    public static function generateUrl()
    {
        $url = str_random(30);

        while (ShareLink::where('url', $url)->exists()) {
            $url = str_random(30);
        }

        return $url;
    }
}
