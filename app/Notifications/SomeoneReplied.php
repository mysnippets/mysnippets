<?php

namespace App\Notifications;

use App\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class SomeoneReplied extends Notification
{
    use Queueable;

    protected $Reply;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Comment $reply)
    {   
        $this->Reply = $reply;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $snippet = $this->Reply->commentable->commentable;
        $user    = $snippet->user;
        
        return [
            "user_id"    => $user->id,
            "avatar"     => $user->avatar,
            "snippet_id" => $snippet->id,
            "comment_id" => $this->Reply->id,
            "subject"    => $this->Reply->user->username . " replied to your comment.",
            "message"    => $this->Reply->message,
            "url"        => route("snippets.show", $snippet->id) . "#comment" . $this->Reply->id
            
        ];
    }

    public function toBroadcast($notifiable)
    {
        $snippet = $this->Reply->commentable->commentable;
        $user    = $snippet->user;

        return new BroadcastMessage([
            "user_id"    => $user->id,
            "avatar"     => $user->avatar,
            "snippet_id" => $snippet->id,
            "comment_id" => $this->Reply->id,
            "subject"    => $this->Reply->user->username . " replied to your comment.",
            "message"    => $this->Reply->message,
            "url"        => route("snippets.show", $snippet->id) . "#comment" . $this->Reply->id
        ]);
    }
}
