<?php

namespace App\Notifications;

use App\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class SomeoneCommented extends Notification
{
    use Queueable;

    protected $Comment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {   
        $this->Comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $snippet = $this->Comment->commentable;
        $user    = $snippet->user;
        
        return [
            "user_id"    => $user->id,
            "avatar"     => $user->avatar,
            "snippet_id" => $snippet->id,
            "comment_id" => $this->Comment->id,
            "subject"    => $this->Comment->user->username . " commented on your snippet.", 
            "message"    => $this->Comment->message,
            "url"        => route("snippets.show", $snippet->id) . "#comment" . $this->Comment->id
        ];
    }

    public function toBroadcast($notifiable)
    {
        $snippet = $this->Comment->commentable;
        $user    = $snippet->user;

        return new BroadcastMessage([
            "user_id"    => $user->id,
            "avatar"     => $user->avatar,
            "snippet_id" => $snippet->id,
            "comment_id" => $this->Comment->id,
            "subject"    => $this->Comment->user->username . " commented on your snippet.", 
            "message"    => $this->Comment->message,
            "url"        => route("snippets.show", $snippet->id) . "#comment" . $this->Comment->id
        ]);
    }
}
