<?php

namespace App\Http\Middleware;


use Auth;
use Closure;

class UsersSnippet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $snippet = $request->route('snippet');

            if (! $snippet->isUsers(Auth::id())) {
                abort(404);
            }
        } catch(Exception $e) {
            abort(404);
        }

        return $next($request);
    }
}
