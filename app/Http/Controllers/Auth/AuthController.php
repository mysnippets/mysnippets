<?php

namespace App\Http\Controllers\Auth;

use Socialite;

use Auth;
use App\User;
use App\SocialUser;
use Illuminate\Http\Request;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)
                        ->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider, Request $request)
    {
        if (!$request->input('code') && !$request->input('oauth_token')) {
            return redirect("/login");
        }

        try {
            $user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return Redirect::to('auth/' . $provider);
        }

        $redirect = $this->HandleSocialLogin($user, $provider);

        return $redirect;

    }

    private function HandleSocialLogin($SocialUser, $provider)
    {
        $Social = SocialUser::ForProvider($provider, $SocialUser->getId())->first();

        if ($Social) {
            Auth::login($Social->user, true);
            return redirect("/");
        }

        if (Auth::user()) {
              Auth::user()->SocialUsers()->create([
                  'provider_id' => $SocialUser->getId(),
                  'provider'    => $provider
              ]);

              return redirect()->back();
        }

        $authUser = User::UniqueForSocial($SocialUser)->first();

        if ($authUser) {
            session(["socialUser" => [
                'user'     => $SocialUser,
                'provider' => $provider
            ]]);

            return redirect()->route("auth.associate");
        }

        Auth::login(User::CreateFromSocial($SocialUser, $provider), true);

        return redirect("/");
    }

    public function Associate()
    {
        return view("auth.login")->with("message", "It appears that there is already an account associated to this email address. Please login to associate your MySnippets.io account with your social account.");
    }

}
