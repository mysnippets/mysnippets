<?php

namespace App\Http\Controllers;

use Auth;
use App\Comment;
use App\Notifications\SomeoneReplied;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Comment $comment)
    {
        $request->validate([
            'message' => 'required'
        ]);
        
        $reply = $comment->replies()->create([
            'message' => $request->message,
            'user_id' => Auth::id()
        ]);

        if (!$reply->user_id !== Auth::id()) {           
            $reply->user->notify(new SomeoneReplied($reply));
        }
        
        return redirect()->route("snippets.show", $comment->commentable_id);
    }

    public function destroy(Comment $comment)
    {
        if ($comment->user_id === Auth::id()) {
            $comment->delete();
        }

        return redirect()->back();
    }
}
