<?php

namespace App\Http\Controllers;

use Auth;
use Notifications;
use App\Snippet;
use App\Comment;
use App\Notifications\SomeoneCommented;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Snippet $snippet)
    {
        $request->validate([
            'message' => 'required'
        ]);
        
        $comment = $snippet->comments()->create([
            'message' => $request->message,
            'user_id' => Auth::id()
        ]);
        
        if (!$comment->user_id !== Auth::id()) {           
            $snippet->user->notify(new SomeoneCommented($comment));
        }

        return redirect()->route("snippets.show", $snippet);
    }

    public function destroy(Comment $comment)
    {
        if ($comment->user_id === Auth::id()) {
            $comment->replies()->delete();
            $comment->delete();
        }

        return redirect()->back();
    }

}
