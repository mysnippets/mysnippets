<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Snippet;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    
    public function index(Request $request)
    {
        $snippets = Snippet::search($request->search)
                           ->where("private", 0);

        $language = $request->only("language");
        $sort     = $request->only("sorting");

        foreach($language as $key => $value) {
            if ($value !== "" && $value !== null) {
                $snippets->where($key, $value);
            }
        }

        foreach($sort as $item) {
            switch($item) {
                case "newest_first":
                    $snippets->orderBy("created_at", "desc");
                    break;
                case "oldest_first":
                    $snippets->orderBy("created_at", "desc");
                    break;
                case "recently_updated":
                    $snippets->orderBy("updated_at", "desc");
                    break;
                case "most_favourited":
                    $snippets->withCount("favouriter")->orderBy("favouriter_count", "desc");
                    break;
            }
        }

        $snippets = $snippets->paginate(4);
        
        $snippets->load("user");

        $title = "Search Results";
                          
        return view('snippets.index', compact('snippets', 'title'));    
    }

}
