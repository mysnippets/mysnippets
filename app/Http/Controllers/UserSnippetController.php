<?php

namespace App\Http\Controllers;

use Auth;
use App\Snippet;
use Illuminate\Http\Request;

class UserSnippetController extends Controller
{

    public function __construct()
    {
        $this->middleware('UsersSnippet')->except(['index', 'create', 'store', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $snippets = Auth::user()
                        ->snippets()
                        ->WithFilters()
                        ->mostRecent()
                        ->paginate(4);

        $title = "My Snippets";

        return view('snippets.index', compact('snippets', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = config('languages');
        $snippet   = new Snippet();

        return view('snippets.create', compact('languages', 'snippet'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'name'        => 'required',
          'description' => 'required',
          'code'        => 'required',
          'language'    => 'required',
          'private'     => 'required'
        ]);

        $snippet = Auth::user()->snippets()->create($request->all());

        $snippet->urls()->create([
          'url' => \App\ShareLink::generateUrl()
        ]);
        
        $snippet->tags = explode(",", $request->tags);

        Snippet::findOrFail($snippet->id)->searchable();
        
        return redirect()->route('snippets.show', $snippet);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Snippet  $snippet
     * @return \Illuminate\Http\Response
     */
    public function show(Snippet $snippet)
    {
        $snippet->load(['comments' => function($query) {
            $query->mostRecent();
            $query->with('replies');
            $query->with('user');
        }])->load("tags");

        
        if ($snippet->private && (Auth::user() && !$snippet->isUsers(Auth::id()))) {
            abort(404);
        }

        $recommended = Snippet::Recommended($snippet)->limit(3)->get();
        
        return view('snippets.show', compact('snippet', 'recommended'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Snippet  $snippet
     * @return \Illuminate\Http\Response
     */
    public function edit(Snippet $snippet)
    {
        $languages = config('languages');

        $snippet->load('tags');

        return view('snippets.edit', compact('snippet', 'languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Snippet  $snippet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Snippet $snippet)
    {
        $request->validate([
          'name'        => 'required',
          'description' => 'required',
          'code'        => 'required',
          'language'    => 'required',
          'private'     => 'required'
        ]);

        $snippet->update($request->all());
        $snippet->tags = explode(",", $request->tags);

        Snippet::findOrFail($snippet->id)->searchable();
        
        return redirect()->route('snippets.show', $snippet);
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Snippet  $snippet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Snippet $snippet)
    {
        $snippet->delete();

        return redirect()->route("home");

    }
}
