<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Snippet;
use App\Favourites;
use Illuminate\Http\Request;

class FavouritesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $snippets = Auth::user()
                        ->Favourites()
                        ->WithFilters()
                        ->paginate(4);

        $title = "Your Favourite Snippets";

        return view('snippets.index', compact('snippets', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Snippet $snippet)
    {
        Auth::user()->favourites()->toggle($snippet);

        return redirect()->back();
    }

}
