<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    
    public function index()
    {
        $notifications = Auth::user()->notifications()->limit(8)->get();

        return response()->json($notifications);
    }

    public function update(Request $request)
    {
        $notifications = Auth::user()->notifications()->limit(8)->get();

        $notifications->markAsRead();

        return response()->json($notifications);
    }

}
