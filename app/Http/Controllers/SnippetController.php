<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Snippet;
use Illuminate\Http\Request;

class SnippetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $snippets = Snippet::public()
                           ->WithFilters()
                           ->mostRecent()
                           ->with("user")
                           ->paginate(4);

        if (Auth::user())
        {
            $userSnippets = Auth::user()->snippets()->mostRecent()->limit(4)->get();
        }

        $title = "Recently Added";

        return view('snippets.index', compact('snippets', "userSnippets", "title"));
    }

}
