<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $snippets = $user->snippets()->paginate(4);

        return view("users.show", compact("user", "snippets"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = Auth::user();

        return view("users.edit", compact("user"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            "bio"     => "nullable|min:10",
            "avatar"  => "image"
        ]);

        $user = Auth::user();

        $user->update([
            "bio"     => $request->bio,
            "website" => $request->website,
        ]);
        
        if (!empty($request->avatar)) {
            $user->update([
                "avatar" => $request->file("avatar")->store("public/images/avatars")
            ]);
        }

        return redirect()->route("user.edit", $user->username);
    }

}
