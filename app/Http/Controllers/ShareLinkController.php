<?php

namespace App\Http\Controllers;

use App\User;
use App\Snippet;
use App\ShareLink;
use Illuminate\Http\Request;

class ShareLinkController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  \App\ShareLink  $shareLink
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, $hash)
    {
        $snippet = $user->snippets()->whereHas("urls", function($query) use ($hash) {
            $query->where("url", $hash);
        })->with('urls')->first();

        $snippet->load(['comments' => function($query) {
            $query->mostRecent();
            $query->with('replies');
            $query->with('user');
        }]);

        $recommended = Snippet::where("language", $snippet->language)->excludeUsers()->mostRecent()->limit(3)->get();

        return view('snippets.show', compact('snippet', 'recommended'));

    }

}
