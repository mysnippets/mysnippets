<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Snippet;
use Illuminate\Http\Request;

class SnippetsController extends \App\Http\Controllers\Controller
{

    public function __construct()
    {
        $this->middleware('UsersSnippet')->except("index");
    }

    public function index() 
    {
        $snippets = Snippet::public()
                           ->WithFilters()
                           ->mostRecent()
                           ->with("user")
                           ->paginate(4);

        return response()->json($snippets);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Snippet  $snippet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Snippet $snippet)
    {
        $snippet->delete();

        return response()->json("snippet delete successfully");

    }
}
