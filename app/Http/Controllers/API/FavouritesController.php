<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Snippet;
use Illuminate\Http\Request;

class FavouritesController extends \App\Http\Controllers\Controller
{
    public function update(Snippet $snippet) 
    {
        Auth::user()->favourites()->toggle($snippet);
        
        return response()->json($snippet);
    }

}
