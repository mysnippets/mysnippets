<?php

namespace App\Http\Controllers\API;

use Auth;
use Notifications;
use App\Snippet;
use App\Comment;
use App\Notifications\SomeoneReplied;
use Illuminate\Http\Request;

class ReplyController extends \App\Http\Controllers\Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Comment $comment)
    {
        $request->validate([
            'message' => 'required'
        ]);
        
        $reply = $comment->replies()->create([
            'message' => $request->message,
            'user_id' => Auth::id()
        ]);
        
        if (!$reply->user_id !== Auth::id()) {           
            $reply->user->notify(new SomeoneReplied($reply));
        }

        return response()->json($comment);
    }

}
