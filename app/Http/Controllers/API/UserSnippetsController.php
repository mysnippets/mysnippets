<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Snippet;
use Illuminate\Http\Request;

class SnippetsController extends \App\Http\Controllers\Controller
{

    public function index(Request $request) 
    {
        if ($request->MySnippets === true) {
            $snippets = Snippet::WithFilters()
                               ->mostRecent()
                               ->with("user")
                               ->paginate(4);
        } else {
            $snippets = Snippet::public()
                               ->WithFilters()
                               ->mostRecent()
                               ->with("user")
                               ->paginate(4);
        }
        
        return response()->json($snippets);
    }

}
