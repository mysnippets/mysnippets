<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class SocialUserController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = Auth::user();

        $user->load("SocialUsers");

        return view("users.social", compact("user"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            "password" => "nullable|confirmed",
            "email"    => "nullable|email"
        ]);

        $user = Auth::user();

        if($request->password !== "" && $request->password !== null)
        {
            $user->password = bcrypt($request->password);
        }

        if($request->email !== "" && $request->email !== null)
        {
            $user->email = $request->email;
        }

        $user->save();

        return redirect()->route("user.settings", $user->username);
    }

    public function destroy(Request $request, User $user)
    {
        Auth::user()->socialUsers()->where("provider", $request->provider)->delete();

        return redirect()->back();
    }
}
