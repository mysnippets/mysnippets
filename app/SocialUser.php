<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialUser extends Model
{
    protected $fillable = [
        'provider',
        'provider_id'
    ];

    public function User()
    {
        return $this->belongsTo("App\User");
    }

    public function scopeForProvider($query, $provider, $provider_id)
    {
        return $query->where('provider', $provider)
                     ->where('provider_id', $provider_id)
                     ->with("user");
    }
}
