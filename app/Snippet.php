<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Snippet extends Model
{
    use Searchable;
    
    protected $fillable = [
      'name',
      'description',
      'code',
      'language',
      'private'
    ];

    protected $appends = array("favourited");

    public function langaugeCode()
    {
        $languages = config("languages");

        return strtolower($languages[$this->language]);
    }

    public function languageName()
    {
        $languages = config("languages");

        return $languages[$this->language];
    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }

    public function urls()
    {
        return $this->hasOne('App\ShareLink');
    }

    public function tags()
    {
        return $this->hasMany('App\Tag');
    }

    public function isUsers($user_id)
    {
        return $user_id === $this->user_id;
    }

    public function comments()
    {
        return $this->morphMany("App\Comment", "commentable");
    }

    public function favouriter()
    {
        return $this->belongsToMany("App\User", "favourites");
    }

    public function scopeExcludeUsers($query)
    {
        if (Auth::user())
        {
            $query->where("user_id", "!=", Auth::id());
        }
    }

    public function scopePublic($query)
    {
        $query->where("private", false);
    }

    public function scopeMostRecent($query)
    {
        $query->orderBy('updated_at', 'desc');
    }

    public function scopeWithFilters($query)
    {
        $request  = request();
        $language = $request->only("language");
        $sort     = $request->only("sorting");

        if (Auth::user()) {
            switch($request->category) {
                case "MySnippets":
                    $query->where("user_id", Auth::id());
                    break;
                case "favourites":
                    $query->whereHas("favouriter", function ($q) use ($request) {
                        $q->where("favourites.user_id", Auth::id());
                    });
                    break;
            }
        }
            
        foreach($language as $key => $value) {
            if ($value !== "" && $value !== null) {
                $query->where($key, $value);
            }
        }

        foreach($sort as $item) {
            switch($item) {
                case "newest_first":
                    $query->orderBy("created_at", "desc");
                    break;
                case "oldest_first":
                    $query->orderBy("created_at", "asc");
                    break;
                case "recently_updated":
                    $query->orderBy("updated_at", "desc");
                    break;
                case "most_favourited":
                    $query->withCount("favouriter")->orderBy("favouriter_count", "desc");
                    break;
            }
        }

    }

    public function scopeRecommended($query, $snippet)
    {
        $query->where("language", $snippet->language)
              ->where("id", "!=" , $snippet->id)
              ->public()
              ->excludeUsers()
              ->mostRecent();
    }

    public function setTagsAttribute($tags) 
    {   
        foreach ($tags as $tag) {
            if ($tag !== "") {
                $this->tags()->firstOrCreate(["value" => $tag]);
            }
        }
    }

    public function hasTags()
    {
        return $this->tags()->count() > 0;
    }

    public function JsonTags()
    {   
        $array = [];

        foreach($this->tags as $tag) {
            array_push($array, $tag->value);
        }
        
        return collect($array)->ToJson();
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {   
        $array = $this->toArray();
        
        foreach($this->tags as $index => $tag) {
            $array["tag" . $index] = $tag->value; 
        }
        
        return $array;
    }

    public function getFavouritedAttribute() 
    {
        if (!Auth::user()) {
            return false;
        }

        return Auth::user()->favourites->where("id", $this->id)->count() > 0;
    }

}
