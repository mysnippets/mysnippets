<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $fillable = [
        "bio", "website", "user_id"
    ];

    public function user()
    {
        return $this->hasOne("App\User");
    }
}
