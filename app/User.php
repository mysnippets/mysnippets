<?php

namespace App;

use Auth;
use Storage;
use Notifications;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
    * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'avatar',
        'bio',
        'website',
    ];

    protected $with = [
        "favourites"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Snippets()
    {
        return $this->hasMany("App\Snippet");
    }

    public function Comments()
    {
        return $this->hasMany("App\Comment");
    }

    public function SocialUsers()
    {
        return $this->hasMany("App\SocialUser");
    }

    public function Favourites()
    {
        return $this->belongsToMany("App\Snippet", "favourites");
    }

    public function getAvatarAttribute($value)
    {
        if (strpos($value, 'http') !== false)
        {
            return $value;
        }

        return ($value !== null && $value !== "") ? Storage::url($value) : asset("images/avatars/placeholder2.png");
    }

    public static function CreateFromSocial($SocialAccount, $provider)
    {
        $username = $SocialAccount->getNickname() ?? strtok($SocialAccount->getEmail(), "@");

        $user = User::create([
            'username'    => $username,
            'email'       => $SocialAccount->getEmail(),
            'avatar'      => $SocialAccount->getAvatar(),
        ]);

        $user->SocialUsers()->create([
            'provider_id' => $SocialAccount->getId(),
            'provider'    => $provider
        ]);

        return $user;
    }

    public function scopeUniqueForSocial($query, $SocialAccount)
    {
        return $query->where('username', $SocialAccount->getNickname())
                     ->orWhere('email',  $SocialAccount->getEmail());
    }

    public function profile()
    {
        return $this->hasOne("App\UserProfile");
    }

    public function isAuth()
    {
        return $this->id === Auth::id();
    }

    public function hasNotifications()
    {
        return $this->unreadNotifications->count() > 0;
    }

}
