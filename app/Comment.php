<?php

namespace App;

use Notifiable;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{   
    protected $fillable = [
        'message',
        'user_id'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function replies() {
        return $this->morphMany("App\Comment", "commentable");
    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }

    public function scopeMostRecent($query)
    {
        $query->orderBy('updated_at', 'desc');
    }
}
