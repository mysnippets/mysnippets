<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
          "username" => "admin",
          "password" => bcrypt("password"),
          "email"    => "wardy484@gmail.com"
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
