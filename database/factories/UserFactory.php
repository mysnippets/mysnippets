<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'username'       => $faker->username,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Snippet::class, function (Faker $faker) {
  $languages = [
      "oeabl",
      "php",
      "javascript"
  ];

    $language = $faker->numberBetween(0, 2);

    switch($language) {
        case 0:
            $code = "<?php echo 'Hello World'; ?>";
            break;
        case 1:
            $code = "console.log('hello world');";
            break;
        case 2:
            $code = "message 'Hello World' view-as alert-box.";
    }

    return [
        'code'           => $code,
        'language'       => $languages[$language],
        'name'           => $faker->name,
        'description'    => $faker->text,
        'private'        => $faker->boolean,
    ];
});
