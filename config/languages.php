<?php

    return [
        "php"        => "PHP",
        "javascript" => "Javascript",
        "csharp"     => "C#",
        "oeabl"      => "Openedge ABL"
    ];
