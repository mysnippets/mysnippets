<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SnippetsTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();

        $this->signIn();

        $this->snippet = $this->user->snippets()
                              ->save(factory("App\Snippet")
                              ->make());
    }

    public function test_snippts_index()
    {
        $this->get("/")
             ->assertSee($this->snippet->name);
    }

    public function test_snippets_show()
    {
        $this->get("snippets/" . $this->snippet->id)
             ->assertSee($this->snippet->name)
             ->assertSee($this->snippet->description)
             ->assertSee(e($this->snippet->code));
    }

    public function test_snippets_store()
    {

    }

}
