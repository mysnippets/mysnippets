<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([ "namespace" => "API"], function() {

    Route::group(["middleware" => ["auth:api"]], function() {
        
        Route::post("/comments/{comment}/reply", "ReplyController@store")->name("api.replies.store");    
        
        Route::delete("/comments/{comment}", "CommentController@destroy")->name("api.replies.destroy");    
        
        Route::post("/snippets/{snippet}/comment", "CommentController@store")->name("api.comment.store");
        
        Route::delete("/snippets/{snippet}", "SnippetsController@destroy")->name("api.snippets.destroy");
        
        Route::put("/snippets/{snippet}/favourite", "FavouritesController@update")->name("api.snippets.favourite");
        
        Route::get("/user-snippets/", "SnippetsController@index")->name("api.usersnippets.index");

    });
    
    Route::get("/snippets/{snippet}/comments", "CommentController@index")->name("api.comment.index");
    Route::get("/snippets/", "SnippetsController@index")->name("api.snippets.index");
    
});

