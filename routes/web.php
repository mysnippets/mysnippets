<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SnippetController@index')->name('home');

Auth::routes();

Route::get('auth/associate', 'Auth\AuthController@associate')->name("auth.associate");
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('search', 'SearchController@index')->name('search');

Route::middleware("auth")->group(function() {

    Route::get('/snippets/create', 'UserSnippetController@create')->name('snippets.create');

    Route::resource('snippets', 'UserSnippetController', ['except' =>
      'show', 'create'
    ]);

    Route::post('snippets/{snippet}/comments', 'CommentController@store')->name("comments.store");
    Route::post('comments/{comment}/', 'ReplyController@store')->name("replies.store");

    Route::delete('comments/{comment}', 'CommentController@destroy')->name("comments.destroy");
    Route::delete('replies/{comment}', 'ReplyController@destroy')->name("replies.destroy");

    Route::get("{user}/favourites", "FavouritesController@index")->name("favourites.index");
    Route::get("favourite/{snippet}", "FavouritesController@store")->name("favourites.store");

    Route::delete("/users/{user}/social", "SocialUserController@destroy")->name("user.social.delete");
    Route::get("/users/{user}/social", "SocialUserController@edit")->name("user.social.edit");

    Route::post("/users/{user}/settings", "AccountSettingsController@update")->name("user.updatesettings");
    Route::get("/users/{user}/settings", "AccountSettingsController@edit")->name("user.account.edit");

    Route::get("/users/{user}/edit", "UserProfileController@edit")->name("user.edit");
    Route::post("/users/{user}", "UserProfileController@update")->name("user.update");

    Route::get("notifications", "NotificationsController@index")->name("notifications.index");
    Route::put("notifications", "NotificationsController@update")->name("notifications.update");

});

Route::get('/users/{user}', 'UserProfileController@show')->name("user.show");

Route::get('/snippets/{snippet}', 'UserSnippetController@show')->name('snippets.show');


Route::get('/{user}/{hash}', 'ShareLinkController@show')->name('snippets.private');

Route::view("/privacy-policy", "bullshit.privacypolicy")->name("privacy");
Route::view("/terms-of-service", "bullshit.termsofservice")->name("service");
Route::view("/copyright", "bullshit.copyright");
 