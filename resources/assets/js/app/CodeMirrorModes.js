export default {
    GetMode(mode) {
        return this.modes[mode];
    },
    modes: {
        "php": {
            mode: "text/x-php",
            file: "codemirror/mode/php/php.js"
        },
        "csharp": {
            mode: "text/x-csharp",
            file: "codemirror/mode/clike/clike.js"
        },
        "javascript": {
            mode: "text/javascript",
            file: "codemirror/mode/javascript/javascript.js"
        },
        "oeabl" : {
            mode: "text/oeabl",
            file: "./modes/oeabl.js"
        }
    }
};