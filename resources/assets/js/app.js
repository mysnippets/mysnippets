
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('clipboard');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('editor', require('./components/Editor.vue'));
Vue.component('fileUpload', require('./components/fileUpload.vue'));
Vue.component('InputTag', require('./components/TagsInput.vue'));
Vue.component('YesNoModal', require('./components/YesNoModal.vue'));
// Vue.component('Snippet', require('./components/Snippet.vue'));
Vue.component('Loading', require('./components/loading.vue'));
Vue.component('Comment', require('./components/comments/Comment.vue'));
Vue.component('CopyToClipboard', require('./components/CopyToClipboard.vue'));
Vue.component('SnippetList', require('./components/SnippetList.vue'));
Vue.component('Paginator', require('./components/Paginator.vue'));


Window.EventHandler = new Vue();

const app = new Vue({
    el: '#app',
    data: {
        user: {},
        notifications: []
    },
    created() {
        axios.get("/notifications")
        .then((response) => {
            this.notifications  = response.data;    
        })
        .catch((error) => {
            console.log("Unable to fetch notifications: " + error);
        });

        axios.get("/api/user")
            .then((response) => {
                this.user = response.data;

                window.Echo.private("App.User." + this.user.id)
                    .notification((notification) => {
                        this.notifications.unshift({
                            "id": notification.id,
                            "data": notification,
                            read_at: null
                        });

                        this.notifications.pop();
                    });
            
            })
            .catch((error) => {
                
            });
    },
    methods: {
        newNotifications(data) {
            this.notifications = data;
        },
    },
    components: {
        Notifications: require("./components/Notifications.vue"),
        Snippet: require("./components/Snippet.vue")
        
    }
});