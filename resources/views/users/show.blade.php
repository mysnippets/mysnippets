@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row">

        <div class="col-md-2 col-xs-12 col-sm-12">
            <img class="img-responsive img-rounded" style="margin-top: 22px" alt="Profile Picture" src="{{  $user->avatar }}">
        </div>

        <div class="col-md-10 col-xs-12 col-sm-12">
            <h2>{{ $user->username }}</h2>
            <p>{{  $user->bio ?? "This user has yet to set up their profile." }}</p>
        </div>

    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <h2>Recent Snippets by {{ $user->username }}</h2>

            @forelse($snippets as $snippet)
                @include("snippets.partials.SnippetCard")
            @empty
                <div class="row">
                    <div class="col-md-12">
                        @if($user->isAuth()) This user is @else You have @endif
                        yet to add any snippets.
                    </div>
                    <br> <br>
                </div>

            @endforelse

            {{ $snippets->links() }} 
        </div>

    </div>
</div>


@endsection
