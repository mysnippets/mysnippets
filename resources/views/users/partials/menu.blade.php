<div class="list-group">
  <a href="{{ route("user.edit", $user->username) }}" class="list-group-item {{  $current === "profile" ? "active" : "" }}">Profile</a>
  <a href="{{ route("user.account.edit", $user->username) }}" class="list-group-item {{  $current === "settings" ? "active" : "" }}">Account Settings</a>
  <a href="{{ route("user.social.edit", $user->username) }}" class="list-group-item {{  $current === "social" ? "active" : "" }}">Social Accounts</a>
</div>
