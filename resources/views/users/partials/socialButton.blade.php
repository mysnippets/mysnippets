@if ($user->SocialUsers->where("provider", $CurrentProvider)->isNotEmpty())
  <i class="fa fa-{{ $CurrentProvider }}"></i>
  <form class="" action="{{ route("user.social.delete", $user->username) }}" method="post">
    {{  csrf_field() }}
    {{  method_field("DELETE") }}
    <input type="hidden" name="provider" value="{{ $CurrentProvider }}">
    <button type="submit" class="btn btn-danger">Unlink</button>
  </form>
@else
  <a href="{{ url('/auth/' . $CurrentProvider) }}"><i class="fa fa-{{ $CurrentProvider }}"></i></a>
@endif
