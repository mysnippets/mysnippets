@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row">

        <div class="col-md-3">
            @include("users.partials.menu", ["current" => "settings"])
        </div>

        <div class="col-md-9">
          <div class="panel panel-default">
              <div class="panel-heading">
                  Edit Account Settings
              </div>

              <form class="" action="{{ route("user.updatesettings", $user->username) }}" method="post">
                  <div class="panel-body">

                      {{ csrf_field() }}

                      @include("errors")
                        <div class="form-group">
                            <label for="">Email Address</label>
                            <input type="email" name="email" value="{{ old("bio", $user->email) }}" class="form-control">
                        </div>
                        
                      <div class="form-group">
                          <label for="">New Password</label>
                          <input name="password" type="password" rows="4" cols="80" class="form-control">
                      </div>

                      <div class="form-group">
                          <label for="">Confirm New Password</label>
                          <input name="confirm_password" type="password" rows="4" cols="80" class="form-control">
                      </div>
                  </div>

                  <div class="panel-footer text-right">
                      <button type="submit" class="btn btn-primary" name="button">Save</button>
                  </div>
             </form>

          </div>
        </div>


    </div>
</div>


@endsection
