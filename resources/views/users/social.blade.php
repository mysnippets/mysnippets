@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row">

        <div class="col-md-3">
            @include("users.partials.menu", ["current" => "social"])
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Social Accounts
                </div>
                <div class="panel-body">
                        Associate Social Accounts with your MySnippets Account
                        <br>
                          <div style="font-size:50px; line-height: 50px; margin-top: 20px;" class="text-center">
                              <div class="col-md-2 col-md-offset-3">
                                  @include("users.partials.socialButton", ["CurrentProvider" => "github"])
                              </div>

                              <div class="col-md-2">
                                  @include("users.partials.socialButton", ["CurrentProvider" => "twitter"])
                              </div>

                              <div class="col-md-2">
                                  @include("users.partials.socialButton", ["CurrentProvider" => "google"])
                              </div>

                          </div>
                </div>
            </div>
        </div>

    </div>
</div>


@endsection
