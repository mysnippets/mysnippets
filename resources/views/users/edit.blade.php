@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row" style="margin-top: 22px">

        <div class="col-md-3">
          @include("users.partials.menu", ["current" => "profile"])
        </div>

        <div class="col-md-9">
          <div class="panel panel-default">
              <div class="panel-heading">
                  Edit Profile
              </div>

              <form class="" action="{{ route("user.update", $user->username) }}" method="post" enctype="multipart/form-data">
                  <div class="panel-body">

                      {{ csrf_field() }}

                      @include("errors")

                      <div class="form-group">
                          <label for="">About You</label>
                          <textarea name="bio" rows="4" cols="80" class="form-control">{{ old("bio", $user->bio ?? "") }}</textarea>
                      </div>

                      <div class="form-group">
                          <label for="">Website</label>
                          <input type="text" name="website" value="{{ old("bio", $user->website ?? "") }}" class="form-control">
                      </div>

                      <div class="form-group">
                          <label for="">Avatar</label>
                          <file-upload image-path="{{ old("avatar", $user->avatar ?? "") }}"></file-upload>
                      </div>
                  </div>

                  <div class="panel-footer text-right">
                      <button type="submit" class="btn btn-primary" name="button">Save</button>
                  </div>
             </form>

          </div>
        </div>
    </div>
</div>


@endsection
