@extends('layouts.app')

@section('content')

    @guest
        @include("auth.signupbanner")
    @endguest

    <div class="container">
        
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="row" style="display: flex; align-items: flex-start; margin-bottom:22px">
                        <div class="col-sm-6">
                            <h2 style="margin-bottom:0px">{{ $title }}</h2>
                        </div>

                        <div class="col-sm-6 text-right" style="align-self: flex-end; margin-bottom: 0px;">
                            <a href="{{ route("snippets.create") }}" class="btn btn-success">Create a Snippet</a>
                        </div>
                    </div>
                </div>  

                <snippet-list string-snippets="{{ $snippets->toJson() }}" 
                                links='{!! $snippets->links() !!}'
                                user="{{ Auth::user() }}"
                                languages="{{ json_encode(config("languages")) }}">
                </snippet-list>

            </div>
        </div>
    </div> 

@endsection
