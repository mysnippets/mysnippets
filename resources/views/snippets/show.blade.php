@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-md-8">

            <h2>{{ $snippet->name }} </h2>
            <p>Created by <a href="{{ route("user.show", $snippet->user->username )}}">{{ $snippet->user->username }}</a> about  {{ $snippet->created_at->diffForHumans() }}</p>

            <input type="hidden" value="{{ $snippet->code }}" id="snippet-code">

            <div class="panel panel-default">
                <div class="">
                  <editor read-only="true"
                          code="{{$snippet->code}}"
                          language="{{ $snippet->language }}"
                          options="false"
                          height="450px"
                          ></editor>
                </div>

                 @if(Auth::id() === $snippet->user_id)
                <div class="panel-footer">
                    <a href="{{ route('snippets.edit', $snippet->id) }}" class="btn btn-success pull-left">Update</a>

                    <form action="{{ route('snippets.destroy', $snippet->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button type="submit" style="margin-left: 5px" class="pull-left btn btn-danger">Delete</button>
                    </form>

                    <div class="" style="clear:both"></div>
                </div>
                @endif

            </div>

            @includeWhen($snippet->isUsers(Auth::id()), "snippets.partials.shareModal")

            <snippet snippet-id="{{ $snippet->id }}" user-id="{{ AUth::id() }}"></snippet>

        </div>

        <div class="col-md-4" style="margin-top: 70px">
            <div class="snippet-description">
                <h3>Description</h3>
                <p>{{ $snippet->description }}</p>
            </div>


            <div class="snippet-options">
                <h3>Options</h3>
                <div class="option-icon-box">
                    <a href="{{ route("favourites.store", $snippet)}}">
                        @if(Auth::user() && Auth::user()->favourites->contains($snippet))
                            <i class="fa fa-star"></i>
                        @else
                            <i class="fa fa-star-o"></i>
                        @endif
                    </a>

                    <button class="no-btn text-primary" data-toggle="modal" data-target="#shareModal">
                        <i class="fa fa-share-square-o" aria-hidden="true"></i>
                    </button>

                    <copy-to-clipboard target-element="code"></copy-to-clipboard>
                </div>
            </div>

            @includeWhen($snippet->hasTags(), "snippets.partials.tags")

        </div>

        @includeWhen(isset($recommended) && $recommended->isNotEmpty(), "snippets.partials.recommended", ["recommended" => $recommended])

    </div>

</div>
@endsection
