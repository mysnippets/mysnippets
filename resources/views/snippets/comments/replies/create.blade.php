

        @include("errors")

        <form class="" action="{{ route('replies.store', $comment) }}" method="post">

            {{ csrf_field() }}
            
            <div class="input-group">
                <input type="text" class="form-control" name="message" value="{{ old('message') }}">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary">Reply</button>
                </span>
            </div>
            
        </form>


