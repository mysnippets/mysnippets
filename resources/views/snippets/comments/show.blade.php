<div class="panel panel-default" id="comment{{ $comment->id }}">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-11">
                <a href="{{ route("user.show", $comment->user->username )}}">{{ $comment->user->username }}</a> says:
            </div>

            <div class="col-md-1 text-right">
                @if($comment->user_id === Auth::id())
                <button type="button" class="pseudobutton" data-toggle="modal" data-target="#DeleteComment{{$comment->id}}"><span class="glyphicon glyphicon-remove"></span></button>

                <yes-no-modal id="DeleteComment{{$comment->id}}" 
                              message="Are you sure you wish to delete this comment?"
                              button-label="Delete"
                              button-class="btn btn-danger"
                              action="{{ route("comments.destroy", $comment) }}"
                              method="delete"
                              csrf="{{ csrf_token() }}"></yes-no-modal>
                @endif
            </div>
        </div>
    </div>

    <div class="panel-body">
        {{ $comment->message }}
    </div>

    <div class="panel-footer">
        @foreach($comment->replies as $reply)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-11">
                            <a href="{{ route("user.show", $reply->user->username )}}">{{ $reply->user->username }}</a> says:
                        </div>

                        <div class="col-md-1 text-right">
                            @if($comment->user_id === Auth::id())
                            <button type="button" class="pseudobutton" data-toggle="modal" data-target="#DeleteComment{{$reply->id}}"><span class="glyphicon glyphicon-remove"></span></button>

                            <yes-no-modal id="DeleteComment{{$reply->id}}" 
                                        message="Are you sure you wish to delete this reply?"
                                        button-label="Delete"
                                        button-class="btn btn-danger"
                                        action="{{ route("comments.destroy", $reply) }}"
                                        method="delete"
                                        csrf="{{ csrf_token() }}"></yes-no-modal>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    {{ $reply->message }}
                </div>
            </div>
        @endforeach

        @include("snippets.comments.replies.create")
    </div>
</div>
