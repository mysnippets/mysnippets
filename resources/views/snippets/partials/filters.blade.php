<div class="row">
	<form class="" action="{{ Route::current()->url }}" method="get">

		@if(! empty(request()->search)) 
            <input type="hidden" value="{{ request()->search }}">
        @endif

		<div class="col-md-8">
			<div class="form-group">
				<div class="input-group">
					<select name="language" class="form-control">
						<option value="">Filter by Language</option>
						@foreach(config("languages") as $key => $lang)
						<option value="{{ $key }}" {{ request()->query("language") == $key ? "selected" : "" }}>{{ $lang }}</option>
						@endforeach
					</select>
					<span class="input-group-btn">
						<button type="submit" class="btn btn-primary filter-sort-btn">filter</button>
					</span>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
				<div class="input-group">
					<select name="sorting" class="form-control">
						<option value="newest_first" {{ request()->query("sorting") == "newest_first" ? "selected" : "" }}>Newest First</option>
						<option value="oldest_first" {{ request()->query("sorting") == "oldest_first" ? "selected" : "" }}>Oldest First</option>
						<option value="recently_updated" {{ request()->query("sorting") == "recently_updated" ? "selected" : "" }}>Most Recently Updated</option>
						<option value="most_favourited" {{ request()->query("sorting") == "most_favourited" ? "selected" : "" }}>Most Favourited</option>
					</select>
					<span class="input-group-btn">
						<button type="submit" class="btn btn-primary filter-sort-btn">Sort</button>
					</span>
				</div>
			</div>
		</div>
	</form>
</div>