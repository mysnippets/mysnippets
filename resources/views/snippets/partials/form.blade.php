{{ csrf_field() }}

<div class="col-md-6">
	<div class="panel panel-default">
		<div class="">
			<editor read-only="false" code="{{ old(" code ", $snippet->code) }}" language="{{ old("language", $snippet->language) }}" 
                    language-list="{{ json_encode($languages) }}" options="true" height="395px"></editor>
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-default ">
		<div class="panel-heading">Meta Data</div>

		<div class="panel-body">
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" name="name" class="form-control" value="{{ old(" name ", $snippet->name)}}">
			</div>

			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" rows="4" cols="80" class="form-control">{{ old("description", $snippet->description) }}</textarea>
			</div>

			<label>Private:</label>
			<input type="hidden" name="private" value="0" />

			<div class="form-group">
				<label class="switch">
					<input type="checkbox" name="private" {{ old( "private", $snippet->private) == 1 ? "checked" : "" }} value="1">
					<span class="slider round"></span>
				</label>
			</div>
			
			<label>Tags:</label>
            <input-tag field-name="tags" default-tags="{{ $snippet->JsonTags() }}"> </input-tag>
		</div>
	</div>
</div>