<div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">


          <div class="col-md-10 col-xs-10">
              <a href="{{ route('snippets.show', $snippet->id) }}">{{ $snippet->name }}</a>
          </div>
          <div class="col-md-2 text-right col-xs-2">
              @if(Auth::user())
              <a href="{{ route("favourites.store", $snippet)}}">
                  @if(Auth::user()->favourites->contains($snippet))
                    <i class="glyphicon glyphicon-star"></i>
                  @else
                    <i class="glyphicon glyphicon-star-empty"></i>
                  @endif

              </a>
              @endif
          </div>
      </div>
    </div>

    <div class="panel-body">
        {{ $snippet->description }}
    </div>

    @if($snippet->isUsers(Auth::id()))
        <div class="panel-footer" style="vertical-align: center">
            <a href="{{ route('snippets.edit', $snippet->id) }}" class="pull-left">Update</a>

            <button type="button" class="text-danger pull-left no-btn" data-toggle="modal" data-target="#DeleteSnippet{{$snippet->id}}">Delete</button>

            <yes-no-modal id="DeleteSnippet{{$snippet->id}}"
                          message="Are you sure you wish to delete this snippet?"
                          button-label="Delete"
                          button-class="btn btn-danger"
                          action="{{ route("snippets.destroy", $snippet) }}"
                          method="delete"
                          csrf="{{ csrf_token() }}"></yes-no-modal>

          <a href="{{ route("home") }}?language={{ $snippet->language }}"><span class="badge badge-info pull-right">{{  $snippet->languageName() }}</span></a>

          <div class="" style="clear:both"></div>

      </div>
    @else
      <div class="panel-footer" style="vertical-align: middle">
          <span class="pull-left">Added by <a href="{{ route("user.show", $snippet->user->username )}}">{{ $snippet->user->username }}</a> about  {{ $snippet->created_at->diffForHumans() }}</span>

           <a href="{{ route("home") }}?language={{ $snippet->language }}"><span class="badge badge-info pull-right">{{  $snippet->languageName() }}</span></a>

          <div style="clear: both;">

          </div>
      </div>
    @endif
</div>
