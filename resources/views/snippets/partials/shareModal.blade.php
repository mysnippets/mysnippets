<div id="shareModal" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">

        <div class="modal-dialog vertical-align-center">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Share with friends...</h4>
            </div>
            <div class="modal-body">
                @if($snippet->private)
                    <input type="text" class="form-control" name="" value="{{ route('snippets.private', [
                        'user' => Auth::user()->username,
                        'hash' => $snippet->urls->url
                        ]) }}"  style="width: 100%" disabled>
                @else
                    <input type="text" class="form-control" name="" value="{{ Request::URL() }}"  style="width: 100%" disabled>
                @endif
            </div>
            </div>

        </div>

    </div>
</div>
