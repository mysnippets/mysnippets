<div class="col-md-4 hidden-xs">
    <h3>Recommended Snippets</h3>

    @foreach($recommended as $snippet)
        @include("snippets.partials.SnippetCard")
    @endforeach

    @if(isset($recommended) && $recommended->isNotEmpty())
        <a href="{{ route("snippets.index") }}">View More...</a>
    @endif
</div>
