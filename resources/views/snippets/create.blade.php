@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Create a Snippet</h2>
        </div>

        @include("errors")

        <form class="" action="{{ route('snippets.store') }}" method="post">

            @include("snippets.partials.form")

            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection

@section("deferred")
  <script type="text/javascript">
     Window.EventHandler.$on("code-change", function(code) {
         document.getElementById("code").value = code;
     });
  </script>
@endsection
