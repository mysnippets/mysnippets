@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row" style="display: flex; align-items: flex-start; margin-bottom:22px">
                <div class="col-sm-12">
                    <h2 style="margin-bottom:0px">Notifications</h2>
                </div>
            </div>
                
            @forelse($notifications as $notification)
                <div class="panel panel-default">
                    <div class="panel-heading">
                            {{ $notification->data["subject"] }}                    
                    </div>

                    <div class="panel-body">
                        {{ $notification->data["message"] }}
                    </div>

                    <div class="panel-footer" style="vertical-align: middle">

                        
                    </div> 
                </div>
            @empty
                <div class="row">
                    <div class="col-md-12">
                        You are yet to receive any notifcations. 
                    </div>
                    <br> <br>
                </div>

            @endforelse



        </div>

    </div>
</div>
@endsection
