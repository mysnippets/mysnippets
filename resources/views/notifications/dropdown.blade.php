@forelse(Auth::user()->notifications as $notification)
    <li>
        <a href="{{ $notification->data["url"] }}">
            {{--  <img src="{{ $notification->data["avatar"] }}" class="img-responsive img-circle pull-left" style="max-height: 30px;" alt="">  --}}
            <span class="">{{ $notification->data["subject"] }}</span>
            
        </a>
    </li>
@empty
    <li><a href="#">No notifications yet</a></li>
@endforelse