@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    @include("errors")

                    @isset($message)
                    <div class="alert alert-info">
                        {{ $message }}
                    </div>
                    @endisset

                    <form id="loginform" class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <input type="hidden" name="g-recaptcha-response" id="captcha">

                        <div class="form-group">
                              <div class="col-sm-8 col-sm-offset-4 col-xs-offset-2">
                                <div class="row">
                                    <button type="submit" class="btn btn-primary g-recaptcha"
                                        data-sitekey="6Lenz0EUAAAAANoevpU3is9mY6XDeYLrlx-EMr-a"
                                        data-callback="CaptchaAuth">
                                        Login
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>


                    <div class="text-center">
                        Alternatively login with...
                        <br>
                        <span style="font-size:50px; width:100%" class="text-center">
                            <a href="{{ url('/auth/github') }}"><i class="fa fa-github"></i></a>
                            <a href="{{ url('/auth/twitter') }}"><i class="fa fa-twitter"></i></a>
                            <a href="{{ url('/auth/google') }}"><i class="fa fa-google"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("deferred")

    <script>
        // Google Captcha Authentication Callback - required to update form and trigger form submission
        function CaptchaAuth(response) {
            document.getElementById("captcha").value = response;
            document.getElementById("loginform").submit();
        }
    </script>

@endsection