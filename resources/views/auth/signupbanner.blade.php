<div class="container-fluid" style="background-color: rgb(3, 39, 90); color: white;">
    <div class="container" style="padding: 15px 0px">
            <div class="col-md-6" style="padding-top: 50px;">
                    <h2 style="color: white">Share, Collaberate and Learn!</h2>
                    <h3>Do you forget things all the time? We do!</h3>
                    <h3>Store useful snippets of code and share them with friends or colleagues.</h3>
                </div>
                <div class="col-md-6 col-offset-md-6">
                        <div class="text-center">                        
        
                                <span style="font-size:50px; width:100%" class="text-center">
                                    <div class="col-md-4">
                                    <a href="{{ url('/auth/github') }}"><i style="color:white;" class="fa fa-github"></i><span style="font-size: 25px; vertical-align:middle; color:white">  Github</span></a>
                                    </div>
                                    <div class="col-md-4">
        
                                    <a href="{{ url('/auth/twitter') }}"><i style="color:white;" class="fa fa-twitter"></i><span style="font-size: 25px; vertical-align:middle; color:white"> Twitter</span></a>
                                </div>
        
                                    <div class="col-md-4">
        
                                    <a href="{{ url('/auth/google') }}"><i style="color:white;" class="fa fa-google"></i><span style="font-size: 25px; vertical-align:middle; color:white">  oogle</span></a>
                                </div>
        
                                </span>
                                <br>
                                or...
                                <br><br>
                              </div>
                        
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
        
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-3 control-label">Name</label>
        
                                    <div class="col-md-9">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
        
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-3 control-label">E-Mail Address</label>
        
                                    <div class="col-md-9">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
        
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-3 control-label">Password</label>
        
                                    <div class="col-md-9">
                                        <input id="password" type="password" class="form-control" name="password" required>
        
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-3 control-label">Confirm Password</label>
        
                                    <div class="col-md-9">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>
        
                                <div class="form-group">
                                    <div class="col-md-2 col-md-offset-3" style="padding-right: 0px">
                                        <button type="submit" class="btn btn-primary " style="width:100%;">
                                            Register
                                        </button>
                                    </div>
                                    <div class="col-md-7" style="color: rgb(169, 169, 169)">

                                            By registering, you agree to the  <a style="color: white;" href="{{ route("privacy") }}">privacy policy</a> and <a href="{{ route("service") }}" style="color: white;">terms of service</a>.
                                    </div>

                                </div>
        
                            </form>
        
                </div>
        
        

    </div>

</div>
