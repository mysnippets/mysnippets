<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="shortcut icon" href="{{ asset("images/favicon.ico") }}">
    <link rel="shortcut icon" href="{{ asset("images/favicon-32.ico") }}" sizes="32x32">

    <title>MySnippets</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
    <div class="Site" id="app">


        <div class="Site-content" v-cloak>
          <nav class="navbar navbar-default navbar-static-top alert-danger" style="min-height: 0px; padding-top: 5px; padding-bottom: 5px; margin-bottom: 0px">
              <div class="container text-center">
                    <b>Warning: </b> This website is currently under development. Your data may be deleted without warning.
              </div>
          </nav>

            <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0px">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}" style="padding-top: 10px">
                            <img src="{{ asset("images/mysnippetslogo.png") }}" alt="MySnippets.io" class="img-responsive" style="max-height: 32px">
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-left">
                          <li>
                              <form action="{{ route('search') }}" method="get">
                                      {{ csrf_field() }}

                                      <div class="input-group search-bar">
                                          <input type="text" name="search" class="form-control" placeholder="Search for..." value="{{ request()->search ?? "" }}">
                                          <span class="input-group-btn">
                                          <button type="submit" class="btn btn-primary search-bar-btn"><span class="glyphicon glyphicon-search"></span></button>
                                      </span>
                                    </div>
                                  <!-- /input-group -->
                              </form>
                          </li>
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @guest
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                            <li><a class="nav-cta" href="{{ route("snippets.create") }}"><i class="fa fa-plus" aria-hidden="true"></i></a></li>

                            <notifications :notifications="notifications" @update="newNotifications"></notifications>

                            
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle nav-profile" data-toggle="dropdown" role="button" aria-expanded="false">

                                        <img src="{{ Auth::user()->avatar }}" class="img-responsive img-circle" style="max-height: 30px;" alt="">
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ route("user.show", Auth::user()->username) }}">Profile</a></li>
                                        <li><a href="{{ route('user.edit', Auth::user()->username) }}">Account Settings</a></li>

                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endguest

                        </ul>
                    </div>
                </div>
            </nav>

            @yield('content')
        </div>

        <footer class="footer"  v-cloak>

            <a href="{{ url("/terms-of-service")}}">Terms of Service</a> |
             <a href="{{ url("privacy-policy")}}">Privacy Policy</a>

        </footer>
    </div>

    <!-- Scripts -->
    @routes

    <script src="{{ mix('js/app.js') }}"></script>
    
    @yield('deferred')
</body>
</html>
